#!/usr/bin/env sh

verb=${1:-build}

cd $(dirname $0)
CGO_ENABLED=0 go $verb -trimpath \
                       -tags "netgo osusergo static_build kvformat" \
                       -ldflags="-s -w -extldflags '-static'" \
                       .
