package main

import (
	"fmt"
	"log"
	"os/exec"
	"sync"
	"time"
)

// TODO: make these env params or cli flags

// notify only about servers that match all of these filters
var SERVER_FILTER = ServerFilter{
	MinPlayers:   1,
	MinFreeSlots: 1,
	Passworded:   OptionalBool(false),
	// Rules:      []string{"TeamDeathmatch"},
}

const CHECK_INTERVAL = 60 * time.Second

// duration after which a notification is shown again for a server that matched already
const REMIND_TIMEOUT = 5 * time.Minute

// duration for which notifications are displayed
const NOTIFICATION_TIMEOUT = 20 * time.Second

func main() {
	state := NewNotificationCache()
	go tick(state)
	for {
		select {
		case <-time.Tick(CHECK_INTERVAL):
			go tick(state)
		}
	}
}

func tick(state NotificationCache) {
	var wg sync.WaitGroup
	wg.Add(2)

	var servers, queues []NotificationSubject
	go func(wg *sync.WaitGroup) {
		var err error
		servers, err = fetchServers()
		if err != nil {
			log.Printf("unable to fetch pubs: %s\n", err)
			return
		}
		state.Update(servers, SERVER_FILTER)
		wg.Done()
	}(&wg)

	go func(wg *sync.WaitGroup) {
		var err error
		queues, err = fetchQueues(true)
		if err != nil {
			log.Printf("unable to fetch queues: %s\n", err)
			return
		}
		state.Update(queues, SERVER_FILTER)
		wg.Done()
	}(&wg)
	wg.Wait()

	log.Printf("fetched %d servers, %d queues\n", len(servers), len(queues))
	for _, s := range servers {
		fmt.Println(s)
	}
	for _, q := range queues {
		if q.Matches(SERVER_FILTER) { // HACK: filter empty queues
			fmt.Println(q)
		}
	}
	state.Notify()
}

func launchSoldat(launchArgs ...string) error {
	cmd := exec.Command("steam", append([]string{"-applaunch", "474220"}, launchArgs...)...)
	// this variant is nice too, but it triggers a confirmation popup, which is just unneccessary.
	// args := url.PathEscape(strings.Join(launchArgs, " "))
	// cmd := exec.Command("steam", fmt.Sprintf("steam://run/474220//%s", args))
	return cmd.Start()
}
