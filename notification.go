package main

// package codeberg.org/norwin/soldat2-tools/s2-players-cli

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

func NewNotificationCache() NotificationCache {
	c := NotificationCache{}
	c.cache = map[string]*ServerNotification{}
	c.lock = &sync.Mutex{}
	return c
}

type NotificationCache struct {
	cache map[string]*ServerNotification
	lock  *sync.Mutex
}
type ServerNotification struct {
	server     NotificationSubject
	notifiedAt time.Time
}

type NotificationSubject interface {
	ID() string
	SendNotification(context.Context) (bool, error)
	Matches(ServerFilter) bool
	LaunchURL() string
}

func (c NotificationCache) Update(servers []NotificationSubject, filter ServerFilter) {
	c.lock.Lock()
	for _, s := range servers {
		if !s.Matches(filter) {
			delete(c.cache, s.ID())
			continue
		}
		if v, ok := c.cache[s.ID()]; ok {
			v.server = s
		} else {
			c.cache[s.ID()] = &ServerNotification{s, time.Time{}}
		}
	}
	c.lock.Unlock()
}

func (c NotificationCache) Notify() {
	ctx, cancel := context.WithTimeout(context.Background(), NOTIFICATION_TIMEOUT)
	for _, n := range c.cache {
		lastNotificationAgo := time.Now().Sub(n.notifiedAt)
		if lastNotificationAgo > REMIND_TIMEOUT {
			n.notifiedAt = time.Now()
			fmt.Printf("  notifying for %s\n", n.server.ID())
			go func(n *ServerNotification) {
				joinRequested, err := n.server.SendNotification(ctx)
				if err != nil {
					fmt.Printf("unable to notify: %s", err)
				}
				if joinRequested {
					cancel() // drop other notifications
					if err := launchSoldat(n.server.LaunchURL()); err != nil {
						fmt.Printf("unable to start S2: %s", err)
					}
				}
			}(n)
		} else {
			fmt.Printf("  reminder for %s due in %s\n", n.server.ID(), REMIND_TIMEOUT-lastNotificationAgo)
		}
	}
}

func (q Queue) SendNotification(ctx context.Context) (bool, error) {
	body := fmt.Sprintf("%d / %d players queued", len(q.PlayerIDs), q.GameSize)
	if q.Players != nil {
		body += "\n" + strings.Join(q.PlayerNames(), ", ")
	}
	title := q.Name()
	stdout, err := execNotifySend(ctx, title, body)
	return string(stdout.Bytes()) == "join\n", err
}

func (s PubServer) SendNotification(ctx context.Context) (bool, error) {
	modifiers := ""
	if len(s.Modifiers) > 0 {
		modifiers = " + " + strings.Join(s.Modifiers, ",")
	}
	body := fmt.Sprintf("%s on %s%s\n%d/%d players",
		s.Rules, s.Level, modifiers,
		s.Players, s.MaxPlayers)

	stdout, err := execNotifySend(ctx, s.Name, body, "--action", "join=🍻 go to the pub!")
	return string(stdout.Bytes()) == "join\n", err
}

func execNotifySend(ctx context.Context, title, body string, args ...string) (*bytes.Buffer, error) {
	allArgs := append(append([]string{
		"--app-name", filepath.Base(os.Args[0]),
		"--category", "presence.online",
		"--icon", "steam_icon_474220", // works on Manjaro KDE
		"--transient",
		// we manage the lifetime of the notification ourselves, to be able to
		// remove notifications on demand (via ctx cancel())
		"--expire-time", "0",
		"--wait",
	}, args...), []string{
		"--",
		title,
		body,
	}...)

	// notify-send cancels a notifiction only on SIGINT, not SIGKILL
	// so we can't use exec.CommandContext() until go 1.20 (https://github.com/golang/go/issues/50436)
	cmd := exec.Command("notify-send", allArgs...)
	var stdout bytes.Buffer
	cmd.Stdout = &stdout
	if err := cmd.Start(); err != nil {
		return nil, err
	}
	go func() {
		<-ctx.Done()
		go func() {
			time.Sleep(1 * time.Second)
			_ = cmd.Process.Signal(os.Kill)
		}()
		cmd.Process.Signal(os.Interrupt)
	}()
	return &stdout, cmd.Wait()
}
