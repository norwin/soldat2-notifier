# s2-players-cli

get notified if servers or queues are populated.
depends on notify-send, so it should work on most linuxes, and possibly osx?

`build.sh` will generate a statically built executable, if you have a recent-ish golang installation in your path.

## license
GPL-3.0-or-later

