package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strings"
	"sync"
	"time"
)

func fetchQueues(resolvePlayers bool) ([]NotificationSubject, error) {
	// "https://stats.soldat2.com:9001/api/v1/game/current"
	req, err := http.Get("https://stats.soldat2.com:9001/api/v1/matchmaking/queue")
	if err != nil {
		return nil, err
	}
	defer req.Body.Close()
	decoder := json.NewDecoder(req.Body)
	var queues []Queue
	if err := decoder.Decode(&queues); err != nil {
		return nil, err
	}
	subjs := make([]NotificationSubject, 0, len(queues))
	for _, q := range queues {
		if resolvePlayers {
			q.Players = make([]*Player, len(q.PlayerIDs))
			for i, p := range q.PlayerIDs {
				var wg sync.WaitGroup
				wg.Add(len(q.PlayerIDs))
				go func(playerID string, i int, wg *sync.WaitGroup) {
					defer wg.Done()
					player, err := fetchPlayer(playerID)
					if err != nil {
						fmt.Printf("couldn't resolve player %s: %s\n", playerID, err)
						return
					}
					q.Players[i] = player
				}(p, i, &wg)
				wg.Wait()
			}
		}
		subjs = append(subjs, q)
	}
	return subjs, nil
}

func fetchServers() ([]NotificationSubject, error) {
	// use 10s caching proxy hosted by fri#3333 to reduce load on the endpoint
	// req, err := http.Get("https://soldat2.com:33070/api/servers")
	req, err := http.Get("https://oczki.pl/s2-players/data/get-public-servers.php")
	if err != nil {
		return nil, err
	}
	defer req.Body.Close()
	decoder := json.NewDecoder(req.Body)
	var servers []PubServer
	if err := decoder.Decode(&servers); err != nil {
		return nil, err
	}
	sort.Sort(ServerList(servers))
	subjs := make([]NotificationSubject, 0, len(servers))
	for _, s := range servers {
		subjs = append(subjs, s)
	}
	return subjs, nil
}

func fetchPlayer(id string) (*Player, error) {
	url := fmt.Sprintf("https://stats.soldat2.com:9001/api/v1/ratings/playfab/%s?withGames=false&withRatingUpdates=false&withPlayerStats=false", id)
	req, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer req.Body.Close()
	decoder := json.NewDecoder(req.Body)
	var player Player
	return &player, decoder.Decode(&player)
}

type Player struct {
	ID   string `json:"playfabId"`
	Name string `json:"displayName"`
}

type ServerFilter struct {
	MinPlayers   int
	MinFreeSlots int
	Passworded   *bool
	Rules        []string
}

type QueueList []Queue
type Queue struct {
	Region         string    `json:"region"`
	GameSize       int       `json:"gameSize"`
	GameMode       Rules     `json:"gameMode"`
	Players        []*Player `json:"-"`
	PlayerIDs      []string  `json:"players"`
	NoEmptyServers bool      `json:"noEmptyServersAvailable"`
}

// these implement NotificationSubject interface
func (q Queue) Matches(f ServerFilter) bool { return len(q.PlayerIDs) != 0 }
func (q Queue) LaunchURL() string           { return "" }
func (q Queue) ID() string {
	return fmt.Sprintf("queue-%s-%s-%d", q.Region, q.GameMode, q.GameSize)
}
func (q Queue) Name() string {
	return fmt.Sprintf("%s %dv%d %s", q.GameMode, q.GameSize/2, q.GameSize/2, strings.ToUpper(q.Region))
}
func (q Queue) PlayerNames() []string {
	names := make([]string, 0, len(q.Players))
	for _, p := range q.Players {
		names = append(names, p.Name)
	}
	return names
}
func (q Queue) String() string {
	players := ""
	if q.Players != nil {
		players = "\t" + strings.Join(q.PlayerNames(), ", ")
	}
	return fmt.Sprintf("%d/%d\t%s%s", len(q.Players), q.GameSize, q.Name(), players)
}

type ServerList []PubServer

// sort interface
func (a ServerList) Len() int           { return len(a) }
func (a ServerList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ServerList) Less(i, j int) bool { return a[i].Players >= a[j].Players }

type PubServer struct {
	Name              string
	Build             string
	Address           string
	IP                string
	Port              int
	WebSocketsAddress string
	WebSocketsPort    int
	Level             string
	Rules             Rules
	Modifiers         Modifiers
	Players           int
	MaxPlayers        int
	Passworded        bool
	TimeAdded         JSUnixTime
	Ping              int
}

func (s PubServer) LaunchURL() string {
	return fmt.Sprintf("s2://%s:%d", s.IP, s.Port)
}

func (s PubServer) String() string {
	return fmt.Sprintf("%d/%d\t%s\t%s", s.Players, s.MaxPlayers, s.Rules, s.Name)
}

func (s PubServer) ID() string {
	return fmt.Sprintf("pub-%s-%d-%s-%v", s.IP, s.Port, s.Name, s.Passworded)
}

func (s PubServer) Matches(f ServerFilter) bool {
	if (s.Players < f.MinPlayers) ||
		(s.MaxPlayers-s.Players < f.MinFreeSlots) ||
		(f.Passworded != nil && s.Passworded != *f.Passworded) {
		return false
	}
	if f.Rules != nil {
		validRules := false
		for _, v := range f.Rules {
			if v == string(s.Rules) {
				validRules = true
				break
			}
		}
		if !validRules {
			return false
		}
	}
	return true
}

type JSUnixTime struct{ time.Time }

func (u *JSUnixTime) UnmarshalJSON(b []byte) error {
	var timestamp int64
	err := json.Unmarshal(b, &timestamp)
	if err != nil {
		return err
	}
	// JS counts milliseconds, not seconds since 1970
	u.Time = time.Unix(timestamp/1000, 0)
	return nil
}

type Modifiers []string

func (m *Modifiers) UnmarshalJSON(b []byte) error {
	var csv string
	err := json.Unmarshal(b, &csv)
	if err != nil {
		return err
	}
	tmp := Modifiers(strings.Split(csv, ", "))
	m = &tmp
	return nil
}

type Rules string

func (r Rules) String() string {
	switch strings.ToLower(string(r)) {
	case "capturetheflag":
		return "CTF"
	case "deathmatch":
		return "DM"
	case "teamdeatchmatch":
		return "TDM"
	case "domination":
		return "DOM"
	default:
		return string(r)
	}
}

type OptionalBoolean *bool

func OptionalBool(val bool) OptionalBoolean {
	var ptr *bool
	ptr = &val
	return ptr
}
